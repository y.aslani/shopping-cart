"use client";

import { CartDataType } from "@/utils/type";
import { useState } from "react";
import Cart from "src/components/cart";
import ProductList from "src/components/productList";

export default function Home() {
  const [cartData, setCardData] = useState<CartDataType>({});
  return (
    <main className="p-4 md:p-8">
      <div className="text-gray-500	mb-4">ثبت سفارش هفتگی</div>
      <div className="grid grid-cols-12 gap-4">
        <div className="col-span-12 md:col-span-8">
          <ProductList cartData={cartData} setCardData={setCardData} />
        </div>
        <div className="col-span-12 md:col-span-4">
          <Cart cartData={cartData} setCardData={setCardData} />
        </div>
      </div>
    </main>
  );
}
