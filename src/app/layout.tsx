"use client";

import "./globals.css";
import type { Metadata } from "next";
import { yekan } from "@/utils/fonts";
import { QueryClientProvider, QueryClient } from "@tanstack/react-query";

export const metadata: Metadata = {
  title: "سبد خرید",
  description: "تست",
};

export default function RootLayout({
  children,
}: {
  children: React.ReactNode;
}) {
  const queryClient = new QueryClient();

  return (
    <html lang="fa" dir="rtl">
      <QueryClientProvider client={queryClient}>
        <body className={`${yekan.className} bg-gray-100`}>{children}</body>
      </QueryClientProvider>
    </html>
  );
}
