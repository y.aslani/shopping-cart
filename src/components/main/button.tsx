import React from "react";
import CustomIcon from "./icon";

interface CustomIconProps {
  title?: string;
  icon?: string;
  className?: string;
  iconClassName?: string;
  onClick: () => void;
}

const Button: React.FC<CustomIconProps> = ({
  title,
  className,
  icon = "",
  iconClassName,
  onClick,
}) => {
  return (
    <button
      onClick={onClick}
      className={` px-2 py-1 rounded flex items-center justify-center ${className}`}
    >
      {title}
      <CustomIcon className={iconClassName} icon={icon} />
    </button>
  );
};

export default Button;
