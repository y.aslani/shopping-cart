import React from "react";
import { Icon } from "@iconify/react";

interface CustomIconProps {
  icon: string;
  className?: string;
  height?: string;
}

const CustomIcon: React.FC<CustomIconProps> = ({
  icon,
  height = 20,
  ...props
}) => {
  return <Icon height={height} icon={icon} {...props} />;
};

export default CustomIcon;
