import { ProductData, cardTypes } from "@/utils/type";
import CustomIcon from "./main/icon";
import { useQueryClient } from "@tanstack/react-query";
import { getDate } from "../utils";
import Button from "./main/button";
import { useCallback } from "react";

const Cart: React.FC<cardTypes> = ({ cartData, setCardData }) => {
  const queryClient = useQueryClient();

  const addToCard = (
    item: ProductData,
    day: string | number,
    counter: number
  ) => {
    const tempCardData = [...cartData[day]];
    const index = tempCardData.findIndex((cardItem) => cardItem.id === item.id);
    const newItem: ProductData = {
      ...item,
      counter: (item.counter ?? 0) + counter,
    };
    tempCardData[index] = { ...newItem };

    setCardData((prev) => ({ ...prev, [day]: tempCardData }));
    queryClient.setQueryData(["productData"], tempCardData);
  };

  const selectedData = useCallback(
    (day: number | string) => {
      return cartData[day]?.filter((item) => item.counter);
    },
    [cartData]
  );

  return (
    <div className="bg-white p-2 md:p-4 rounded-md ">
      <div className="flex justify-between text-sm  mb-4 font-semibold">
        <div className="flex items-center ">
          <CustomIcon icon="ic:outline-shopping-basket" className="ml-2" />
          موارد انتخابی
        </div>
        <div className="flex items-center text-gray-400	">
          <CustomIcon icon="ic:round-delete" className="ml-2" />
          حذف همه
        </div>
      </div>

      {Object.keys(cartData)?.map((day: number | string) =>
        selectedData(day).length ? (
          <div
            key={day}
            className="border border-gray-100  rounded-md text-sm mb-4"
          >
            <div className="p-2 bg-gray-100 flex justify-between ">
              <div className="font-bold">
                <span className="ml-2">{getDate(day)}</span>
              </div>
              <div className="flex items-center text-gray-400	">
                <span>100 تومان</span>
                <span className="mx-2">25 کیلو</span>
                <CustomIcon height="15" icon="ic:round-delete" />
              </div>
            </div>
            {selectedData(day).map((product: ProductData, index) => (
              <div className="px-2 pt-2 text-sm" key={product.id}>
                <div className="font-bold">{product.name}</div>
                <div className="flex justify-between">
                  <span>{product.price} تومان</span>
                  <div className="flex my-2">
                    <Button
                      className="text-white bg-green-600 text-sm"
                      iconClassName="h-4"
                      icon="ic:baseline-add"
                      onClick={() => addToCard(product, day, 1)}
                    />
                    <span className="mx-8">{product.counter}</span>

                    <Button
                      className="bg-white text-green-600 text-sm"
                      iconClassName="h-4"
                      icon="ic:round-minus"
                      onClick={() => addToCard(product, day, -1)}
                    />
                  </div>
                </div>
                {index !== selectedData(day).length - 1 && (
                  <hr className="border-1 border-gray-300 " />
                )}
              </div>
            ))}
          </div>
        ) : (
          <></>
        )
      )}
      <div className="">
        <Button
          className="w-full  bg-green-600 text-white mx-auto"
          iconClassName="h-4"
          title="انتخاب زمان و آدرس"
          onClick={() => {
            const transformedData = Object.entries(cartData).map(([day]) => {
              const delivery_date = getDate(day, "server");
              const order_product = selectedData(day).map((product) => ({
                id: product.id,
                name: product.name,
                quantity: product.counter || 1, // You need to specify the quantity logic
                price: product.price,
              }));
              return {
                delivery_date,
                order_product,
              };
            });

            console.log(transformedData);
          }}
        />
      </div>
    </div>
  );
};

export default Cart;
