import Button from "../main/button";
import CustomIcon from "../main/icon";
import WeekDays from "./weekDays";
import { useQuery, useQueryClient } from "@tanstack/react-query";
import { ProductData, cardTypes } from "@/utils/type";
import { fetchMockData } from "@/utils/services";
import { useState, useEffect } from "react";
import moment from "jalali-moment";

const ProductList: React.FC<cardTypes> = ({ cartData, setCardData }) => {
  const queryClient = useQueryClient();
  const { data, refetch } = useQuery<ProductData[] | undefined>({
    queryKey: ["productData"],
    queryFn: fetchMockData,
  });

  const [selectedDay, setSelectedDay] = useState<number>(
    +moment().format("jD")
  );

  const addToCard = (item: ProductData, counter: number) => {
    const tempCardData = [...(cartData[selectedDay] ?? data)];
    const index = tempCardData.findIndex((cardItem) => cardItem.id === item.id);
    const newItem: ProductData = {
      ...item,
      counter: (item.counter ?? 0) + counter,
    };
    tempCardData[index] = { ...newItem };

    setCardData((prev) => ({ ...prev, [selectedDay]: tempCardData }));
    queryClient.setQueryData(["productData"], tempCardData);
  };

  useEffect(() => {
    if (!cartData[selectedDay]?.length) refetch();
    else queryClient.setQueryData(["productData"], cartData[selectedDay]);
  }, [selectedDay, refetch, queryClient, cartData]);

  return (
    <div>
      <WeekDays selectedDay={selectedDay} setSelectedDay={setSelectedDay} />
      <div className="mt-4 bg-white rounded-md p-2 md:p-4">
        <div className="flex items-center text-sm mb-4 font-semibold">
          <CustomIcon icon="mdi:cart-outline" className="ml-2" />
          پسماندهای مناسب با کسب و کار شما
        </div>
        <div className="flex flex-wrap gap-2">
          {data?.map((item: ProductData) => (
            <div
              key={item.id}
              className="w-[22rem] bg-gray-100 rounded-lg p-2	md:p-3"
            >
              <div className="flex ">
                <div>
                  <img
                    alt="test"
                    className="h-full w-12 object-cover"
                    src={item.imageUrl}
                  />
                </div>
                <div className="px-2 ">
                  <div className="text-sm font-bold">{item.name}</div>
                  <div className="text-xs my-1">{item.content}</div>
                  <div className="text-xs">
                    <div className="flex ">
                      <div className="flex font-bold">
                        <CustomIcon
                          height="12"
                          className="ml-1"
                          icon="mdi:card-multiple-outline"
                        />
                        <span>{item.price} تومان</span>
                        <span className="mr-4">({item.faire} کیلو)</span>
                      </div>
                    </div>
                    <div className="flex items-center mt-2">
                      {item.counter ? (
                        <>
                          <Button
                            className="text-white bg-green-600 text-sm"
                            iconClassName="h-4"
                            icon="ic:baseline-add"
                            onClick={() => addToCard(item, 1)}
                          />
                          <span className="mx-8">{item.counter ?? 0}</span>

                          <Button
                            className="bg-white text-green-600 text-sm"
                            iconClassName="h-4"
                            icon="ic:round-minus"
                            onClick={() => addToCard(item, -1)}
                          />
                        </>
                      ) : (
                        <Button
                          className="text-white bg-green-600 text-sm "
                          iconClassName="mr-2 h-4"
                          onClick={() => addToCard(item, 1)}
                          title="افزودن به سبد"
                        />
                      )}
                    </div>
                  </div>
                </div>
              </div>
              <hr className="border-1 border-gray-300 my-2" />
              <Button
                className="text-indigo-600 text-sm m-auto"
                iconClassName="mr-2 h-4"
                icon="icon-park-outline:left"
                onClick={() => {}}
                title="توضیحات محصول"
              />
            </div>
          ))}
        </div>
      </div>
    </div>
  );
};

export default ProductList;
