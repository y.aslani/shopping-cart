import React, { useMemo, Dispatch, SetStateAction } from "react";
import moment, { Moment } from "jalali-moment";

interface Props {
  selectedDay: number;
  setSelectedDay: Dispatch<SetStateAction<number>>;
}

const WeekDays: React.FC<Props> = ({ selectedDay, setSelectedDay }) => {
  const weekDates: { name: string; date: Moment; id: number }[] =
    useMemo(() => {
      const startOfWeek = moment().startOf("week");
      return Array.from({ length: 7 }, (_, index) => {
        const date = startOfWeek.clone().add(index - 1, "days");
        return {
          id: +moment(date).format("jD"),
          date,
          name: date.locale("fa").format("dddd"),
        };
      });
    }, []);


  return (
    <div className="flex gap-2 ">
      {weekDates.map((day) => (
        <div
          key={day.id}
          onClick={() => {
            setSelectedDay(day.id);
          }}
          className={`${
            day.id === selectedDay
              ? "bg-blue-600 text-white"
              : "bg-white cursor-pointer"
          } flex-1 flex flex-col items-center rounded-md py-2 md:py-4 `}
        >
          <div>{day.name}</div>
          <div
            className={
              true
                ? "rounded-full border border-white w-8 h-8 flex items-center justify-center"
                : ""
            }
          >
            {day.date.format("jD")}
          </div>
        </div>
      ))}
    </div>
  );
};

export default WeekDays;
