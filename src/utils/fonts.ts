import localFont from "next/font/local";

export const yekan = localFont({
  src: [
    {
      path: "../assets/fonts/IRANSansWeb_Light.woff2",
      weight: "100",
      style: "normal",
    },
    {
      path: "../assets/fonts/IRANSansWeb.woff2",
      weight: "200",
      style: "normal",
    },
  ],
});
