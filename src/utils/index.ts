import moment from "jalali-moment";

export const getDate = (day: number | string, type: string) => {
  const currentMonth = moment().format(type ? "M" : "jM");
  const currentYear = moment().format(type ? "YYYY" : "jYYYY");

  return `${currentYear}/${currentMonth}/${day}`;
};
