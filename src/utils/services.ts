import axios from "axios";
import { ProductData } from "./type";

export const fetchMockData = async (): Promise<ProductData[]> => {
  const response = await axios.get<ProductData[]>("/data.json");
  return response.data;
};
