import { Dispatch, SetStateAction } from "react";

export interface ProductData {
  id: number;
  name: string;
  email: string;
  price: number;
  imageUrl: string;
  faire: number;
  content: string;
  counter?: number;
}

export type CartDataType = {
  [key: number | string]: ProductData[];
};

export interface cardTypes {
  cartData: CartDataType;
  setCardData: Dispatch<SetStateAction<CartDataType>>;
}
